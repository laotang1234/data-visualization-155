// 上面这个代码处理过度动画（默认加上不用管）
document.addEventListener('DOMContentLoaded', () => {
  setTimeout(() => {
    document.body.classList.add('sidenav-pinned')
    document.body.classList.add('ready')
  }, 200)
})

// 退出功能
document.querySelector('#logout')?.addEventListener('click', function () {
  // 1. 移除token
  localStorage.removeItem('token')
  // 2. 跳转到登录页
  location.href = './login.html' // 路径和JS文件在哪里无关；和html文件有关
})

// 加入axios的配置
// 配置请求根路径，参考地址：https://www.axios-http.cn/docs/config_defaults
axios.defaults.baseURL = 'http://ajax-api.itheima.net'

// 配置请求头，参考地址：https://www.axios-http.cn/docs/config_defaults
axios.defaults.headers.common['Authorization'] = localStorage.getItem('token')

// 添加响应拦截器
axios.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。成功进入这个函数
    // 对响应数据做点什么
    return response;
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。失败进入这个函数
    // 对响应错误做点什么
    // console.dir(error)
    if (error.response.status === 401) {
      // 说明 token 有问题了（不是忘记携带了，就是token过期了）
      location.href = './login.html'
    }
    return Promise.reject(error);
  }
);