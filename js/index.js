// ------------------- 折线图 --------------------------------
function lineChart(a) {
  // console.log(a)
  let myChart = echarts.init(document.querySelector('#line'))
  let option = {
    title: {
      text: '2022全学科薪资走势',
      top: 15,
      left: 10,
      textStyle: {
        fontSize: 16
      }
    },
    xAxis: {
      type: 'category',
      data: a.map(item => item.month),
      // data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
      axisLabel: {
        color: '#999' // 轴线下面的文字颜色
      },
      axisLine: {
        lineStyle: {
          color: '#ccc', // 轴线颜色
          type: 'dashed' // 轴线类型（虚线）
        }
      }
    },
    yAxis: {
      type: 'value',
      splitLine: {
        lineStyle: {
          type: 'dashed' // Y轴分割线类型（虚线）
        }
      }
    },
    // 提示框组件
    tooltip: {
      // 提示的触发方式
      // 默认是 item，鼠标放到每一项上才能提示
      // 可选 axis ，鼠标放到轴上即可提示
      trigger: 'axis',
    },
    // 网格设置
    grid: {
      top: '20%'
    },
    series: [
      {
        data: a.map(item => item.salary),
        // data: [9000, 12000, 15000, 13000, 10000, 18000, 14000, 10000, 12000, 13000, 15000, 19000],
        type: 'line',
        smooth: true, // 平滑曲线
        lineStyle: {
          width: 6 // 线条粗细
        },
        // symbol: ''
        symbolSize: 10, // 小圆圈的大小
        areaStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0, color: '#499FEE' // 0% 处的颜色
            }, {
              offset: 0.8, color: 'rgba(255,255,255,0.2)' // 80% 处的颜色
            }, {
              offset: 1, color: 'rgba(255,255,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      }
    ],
    // 线条的颜色
    color: {
      type: 'linear',
      x: 0,
      y: 0,
      x2: 0,
      y2: 1,
      colorStops: [{
        offset: 0, color: '#499FEE' // 0% 处的颜色
      }, {
        offset: 1, color: '#5D75F0' // 100% 处的颜色
      }],
      global: false // 缺省为 false
    }
  }
  myChart.setOption(option) // 第1个O是大写的
}
// lineChart() // 等接口返回数据之后，再调用


// ------------------- 饼图（右上角） --------------------------
function classSalaryChart(a) {
  // console.log(a)
  let myChart = echarts.init(document.querySelector('#salary'))
  let option = {
    title: {
      text: '班级薪资分布',
      top: 15,
      left: 10,
      textStyle: {
        fontSize: 16
      }
    },
    tooltip: {
      trigger: 'item'
    },
    legend: {
      bottom: '6%',
      left: 'center'
    },
    color: ['#FDA224', '#5097FF', '#3ABCFA', '#34D39A'],
    series: [
      {
        name: '班级薪资分布', // 鼠标移入提示这个名字
        type: 'pie', // 表示图表的类型是 饼图
        radius: ['50%', '64%'], // 饼的半径
        itemStyle: {
          borderRadius: 10, // 每一项的圆角
          borderColor: '#fff',
          borderWidth: 2
        },
        label: {
          show: false, // 控制标签（文本）不显示
        },
        labelLine: {
          show: false
        },
        data: a.map(item => {
          // return { value: 20, name: '一万以下' }
          return { value: item.g_count + item.b_count, name: item.label }
        })
        // data: [
        //   { value: 1048, name: '>1万' },
        //   { value: 735, name: '1~1.5万' },
        //   { value: 580, name: '1.5-2万' },
        //   { value: 484, name: '>2万' }
        // ]
      }
    ]
  }
  myChart.setOption(option) // 第1个O是大写的
}
// classSalaryChart()


// ------------------- 柱状图 --------------------------------
// 作业
function groupSalaryChart(a) {
  // console.log(a)
  let myChart = echarts.init(document.querySelector('#lines'))
  let option = {
    xAxis: {
      type: 'category',
      // data: ['张三', '李四', '王五', '4月', '5月', '6月', '7月', '8月'],
      data: a[1].map(item => item.name),
      axisLabel: {
        color: '#999' // 轴线下面的文字颜色
      },
      axisLine: {
        lineStyle: {
          color: '#ccc', // 轴线颜色
          type: 'dashed' // 轴线类型（虚线）
        }
      }
    },
    yAxis: {
      type: 'value',
      splitLine: {
        lineStyle: {
          type: 'dashed' // Y轴分割线类型（虚线）
        }
      }
    },
    tooltip: {},
    series: [
      {
        data: a[1].map(item => item.hope_salary),
        type: 'bar',
        name: '期望薪资' // 这个数据的名字，可以在鼠标移入的提示上显示
      },
      {
        data: a[1].map(item => item.salary),
        type: 'bar',
        name: '就业薪资' // 这个数据的名字，可以在鼠标移入的提示上显示
      }
    ],
    color: [{
      type: 'linear',
      x: 0,
      y: 0,
      x2: 0,
      y2: 1,
      colorStops: [{
        offset: 0, color: '#34D39A' // 0% 处的颜色
      }, {
        offset: 1, color: 'rgba(52,211,154,0.2)' // 100% 处的颜色
      }],
      global: false // 缺省为 false
    }, {
      type: 'linear',
      x: 0,
      y: 0,
      x2: 0,
      y2: 1,
      colorStops: [{
        offset: 0, color: '#499FEE' // 0% 处的颜色
      }, {
        offset: 1, color: 'rgba(73,159,238,0.2)' // 100% 处的颜色
      }],
      global: false // 缺省为 false
    }],
    grid: {
      top: 30,
      left: 70,
      right: 30,
      bottom: 50
    }
  }
  myChart.setOption(option)

  // 给柱状图上面的 8 个按钮，注册click事件
  document.querySelector('#btns').addEventListener('click', function (e) {
    if (e.target.tagName === 'BUTTON') {
      // 排他效果
      document.querySelector('#btns .btn-blue').classList.remove('btn-blue')
      e.target.classList.add('btn-blue')
      // echarts更换图表数据的步骤【1. 更换图表中x轴、series数据   2. myChart.setOption(option)重新创建图表即可】
      // 获取组号
      let i = e.target.innerHTML.trim() // trim()是去掉字符串两边的空白
      // console.log(i) // 组号有了，每组的数据就是  a[i]
      // 换图表配置项中的数据
      option.xAxis.data = a[i].map(item => item.name)
      option.series[0].data = a[i].map(item => item.hope_salary)
      option.series[1].data = a[i].map(item => item.salary)

      myChart.setOption(option)
    }
  })
}
// groupSalaryChart()


// ------------------- 饼图（左下角） --------------------------
function sexSalaryChart(a) {
  let myChart = echarts.init(document.querySelector('#gender'))
  let option = {
    title: [
      {
        text: '男女生薪资分布',
        top: 15,
        left: 10,
        textStyle: {
          fontSize: 16
        }
      },
      {
        text: '男生',
        top: '50%',
        left: '45%',
        textStyle: {
          fontSize: 12
        }
      },
      {
        text: '女生',
        top: '85%',
        left: '45%',
        textStyle: {
          fontSize: 12
        }
      }
    ],
    tooltip: {
      trigger: 'item'
    },
    color: ['#FDA224', '#5097FF', '#3ABCFA', '#34D39A'],
    series: [
      {
        name: '男生', // 鼠标移入提示这个名字
        type: 'pie', // 表示图表的类型是 饼图
        radius: ['20%', '30%'], // 饼的半径
        center: ['50%', '30%'], // 圆心点
        data: a.map(item => {
          return { value: item.b_count, name: item.label }
        })
      },
      {
        name: '女生', // 鼠标移入提示这个名字
        type: 'pie', // 表示图表的类型是 饼图
        radius: ['20%', '30%'], // 饼的半径
        center: ['50%', '70%'], // 圆心点
        data: a.map(item => {
          return { value: item.g_count, name: item.label }
        })
      }
    ]
  }
  myChart.setOption(option)
}

// sexSalaryChart()


// ------------------- 地图 ----------------------------------
function mapChart(a) {
  // console.log(a)
  const mapData = [
    { name: '南海诸岛', value: 0 },
    { name: '北京', value: 0 },
    { name: '天津', value: 0 },
    { name: '上海', value: 0 },
    { name: '重庆', value: 0 },
    { name: '河北', value: 0 },
    { name: '河南', value: 0 },
    { name: '云南', value: 0 },
    { name: '辽宁', value: 0 },
    { name: '黑龙江', value: 0 },
    { name: '湖南', value: 0 },
    { name: '安徽', value: 0 },
    { name: '山东', value: 0 },
    { name: '新疆', value: 0 },
    { name: '江苏', value: 0 },
    { name: '浙江', value: 0 },
    { name: '江西', value: 0 },
    { name: '湖北', value: 0 },
    { name: '广西', value: 0 },
    { name: '甘肃', value: 0 },
    { name: '山西', value: 0 },
    { name: '内蒙古', value: 0 },
    { name: '陕西', value: 0 },
    { name: '吉林', value: 0 },
    { name: '福建', value: 0 },
    { name: '贵州', value: 0 },
    { name: '广东', value: 0 },
    { name: '青海', value: 0 },
    { name: '西藏', value: 0 },
    { name: '四川', value: 0 },
    { name: '宁夏', value: 0 },
    { name: '海南', value: 0 },
    { name: '台湾', value: 0 },
    { name: '香港', value: 0 },
    { name: '澳门', value: 0 }
  ]

  // 在模板数据、接口返回的数据 之后。融合两部分数据
  mapData.forEach(item => {
    // 判断接口返回的数据有没有北京、河北的、内蒙古的
    let 结果 = a.find(v => {
      return v.name.includes(item.name)
    })
    // console.log(结果) 
    if (结果) {
      // item.value = 接口返回的value
      item.value = 结果.value
    }
  })

  let myChart = echarts.init(document.querySelector('#map'))
  let option = {
    // 从 0 开始，自己写配置
    series: [
      {
        name: '籍贯分布',
        type: 'map', // type表示图表的类型是 地图
        map: 'china', // map表示做的是 中国地图
        // 有了以上两项，最起码地图可以显示出来了
        data: mapData, // 地图使用的数据
        label: {
          show: true, // 显示 省的名字
          fontSize: 10 // 文字大小
        },
        itemStyle: { // 默认每个省的样式
          areaColor: '#E0FFFF', // 每个省的颜色
          borderColor: 'rgba(0, 0, 0, 0.2)'
        },
        emphasis: { // 鼠标移入之后，每个省的样式
          itemStyle: {
            areaColor: '#34D39A',
            borderWidth: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)',
            shadowBlur: 20,
            shadowOffsetX: 0,
            shadowOffsetY: 0
          }
        }
      }
    ],
    // 视觉映射组件
    visualMap: {
      type: 'continuous', // 表示连续型
      max: 7,
      min: 0,
      text: [7, 0],
      left: 15,
      bottom: 10,
      inRange: { color: ['#fff', '#0075F0'] }  // 从小到大的颜色分别为：#fff 和 #0075F0
    },
    // 鼠标移入提示
    tooltip: {
      // 下面的 fotmatter用于 自定义提示框的内容
      // {a} 表示的就是 series图形的 name值
      // {b} 表示数据中的 name 值
      // {c} 表示数据中的 value 值
      formatter: '{b}: {c} 位学员',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      borderWidth: 0,
      textStyle: {
        color: '#fff'
      }
    }
  }
  myChart.setOption(option)
}
// mapChart()

// ====================================================================================
// 发送请求，获取接口数据，把数据渲染到页面中，把数据用到图表中
axios({
  url: '/dashboard',
}).then(result => {
  // console.log(result.data.data)
  let { groupData, overview, provinceData, salaryData, year } = result.data.data
  // console.log(overview)
  // console.log(overview.salary) // 解构之后，写起来比较简短
  // console.log(result.data.data.overview.salary) // 不解构也能用，就是写起来有点长
  // 1. 处理 概览区域 数据渲染
  for (let key in overview) {
    document.querySelector(`[name=${key}]`).innerHTML = overview[key]
  }
  // 2. 处理折线图的数据
  lineChart(year)
  // 3. 两个饼图数据处理
  classSalaryChart(salaryData)
  sexSalaryChart(salaryData)
  // 4. 处理柱状图数据
  groupSalaryChart(groupData)
  // 5. 地图数据处理
  mapChart(provinceData)
})