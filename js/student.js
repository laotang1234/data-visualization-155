// ----------------- 获取学员列表数据，并渲染到表格 -----------------------------
function renderStudent() {
  axios({
    url: '/students',
  }).then(result => {
    // console.log(result)
    let newArr = result.data.data.map(item => {
      return `
        <tr>
          <td>${item.name}</td>
          <td>${item.age}</td>
          <td>${item.gender ? '女' : '男'}</td>
          <td>第${item.group}组</td>
          <td>${item.hope_salary}</td>
          <td>${item.salary}</td>
          <td>${item.province} ${item.city} ${item.area}</td>
          <td>
            <a href="javascript:;" class="text-success mr-3">
              <i class="bi bi-pen" data-id="${item.id}"></i>
            </a>
            <a href="javascript:;" class="text-danger">
              <i class="bi bi-trash" data-id="${item.id}"></i>
            </a>
          </td>
        </tr>
      `
    })
    document.querySelector('tbody').innerHTML = newArr.join('')
  })
}

renderStudent()

// --------------------------- 省市县联动 ---------------------------------
let sheng = '<option value="">--省份--</option>'
let shi = '<option value="">--城市--</option>'
let xian = '<option value="">--地区--</option>'

let province = document.querySelector('[name=province]') // 下拉框
let city = document.querySelector('[name=city]')
let area = document.querySelector('[name=area]')
// 获取省，并渲染
axios({
  url: '/api/province'
}).then(result => {
  // console.log(result)
  let newArr = result.data.data.map(item => `<option value="${item}">${item}</option>`)
  province.innerHTML = sheng + newArr.join('')
})
// 省切换的时候，获取市，并渲染
province.addEventListener('change', function () {
  // console.log(province.value)
  area.innerHTML = xian // 切换省的时候，重置区县
  axios({
    url: '/api/city',
    params: {
      pname: province.value
    }
  }).then(result => {
    let newArr = result.data.data.map(item => `<option value="${item}">${item}</option>`)
    city.innerHTML = shi + newArr.join('')
  })
})
// 市切换的时候，获取区县，并渲染
city.addEventListener('change', function () {
  axios({
    url: '/api/area',
    params: {
      pname: province.value,
      cname: city.value
    }
  }).then(result => {
    let newArr = result.data.data.map(item => `<option value="${item}">${item}</option>`)
    area.innerHTML = xian + newArr.join('')
  })
})

// --------------------------- 添加学员 -----------------------------------
// 模态框盒子 在 student.html 第 205~285 行
let addModal = new bootstrap.Modal(document.querySelector('#modal'))

// 点击 + 的时候，让模态框显示
document.querySelector('#openModal').addEventListener('click', function () {
  document.querySelector('.modal-title').innerHTML = '添加学员'
  addModal.show() // 让模态框显示
})

// 点击模态框中的 确认 按钮
document.querySelector('#submit').addEventListener('click', async function () {
  // 获取表单各项的值
  // let data = val(表单)
  let data = val(document.querySelector('#form'))
  // 检查是否是接口需要的数据
  // console.log(data)
  data.age = +data.age
  data.group = +data.group
  data.gender = +data.gender
  data.hope_salary = +data.hope_salary
  data.salary = +data.salary
  // console.log(data)
  let title = document.querySelector('.modal-title').innerHTML
  let result
  if (title === '添加学员') {
    // Ajax提交，添加数据
    result = await axios({
      method: 'POST',
      url: '/students',
      data: data
    })
  } else if (title === '修改学员') {
    // Ajax提交，修改数据
    let id = document.querySelector('.modal-title').dataset.id
    result = await axios({
      method: 'PUT',
      url: `/students/${id}`,
      data: data
    })
  }
  message.success(result.data.message) // 使用插件提示消息
  document.querySelector('#form').reset() // 重置表单
  renderStudent() // 更新页面数据
  addModal.hide() // 让模态框隐藏
})


// 找到tbody注册点击事件，里面判断点击的是删除，还是编辑
document.querySelector('tbody').addEventListener('click', async function (e) {
  if (e.target.classList.contains('bi-trash')) {
    // 说明点击了删除
    // console.log('你点击了删除')
    let id = e.target.dataset.id
    // 发送ajax请求，进行删除操作
    axios({
      url: `/students/${id}`,
      method: 'DELETE'
    }).then(result => {
      // console.log(result)
      message.success('删除成功') // 提示
      renderStudent() // 更新页面数据
    })
  }

  if (e.target.classList.contains('bi-pen')) {
    // 说明点击了编辑
    // console.log('你点击了编辑')
    let id = e.target.dataset.id // 获取编辑按钮的自定义属性值  值是 2211396
    document.querySelector('.modal-title').innerHTML = '修改学员'
    // 下面是设置模态框标题(h5)的自定义属性，值是当前修改的学员的id
    // document.querySelector('.modal-title').setAttribute('属性名', 值)
    document.querySelector('.modal-title').setAttribute('data-id', id)
    addModal.show() // 显示模态框
    // 发送请求，获取当前修改的这个学员的详细信息
    let result = await axios({ url: `/students/${id}` })
    console.log(result.data.data) // 输出的是 个人的详细信息
    // 获取城市 和 区县，渲染到第2个和第3个下拉框的位置
    let res1 = await axios({
      url: '/api/city',
      params: {
        pname: result.data.data.province
      }
    })
    let res2 = await axios({
      url: '/api/area',
      params: {
        pname: result.data.data.province,
        cname: result.data.data.city
      }
    })
    // console.log(res1)
    let newArr1 = res1.data.data.map(item => `<option value="${item}">${item}</option>`)
    city.innerHTML = shi + newArr1.join('')
    // console.log(res2)
    let newArr2 = res2.data.data.map(item => `<option value="${item}">${item}</option>`)
    area.innerHTML = xian + newArr2.join('')
    // 做数据回填，继续使用插件 【 val(表单, 数据) 】，关于这个插件，只是简化代码的步骤。
    val(document.querySelector('#form'), result.data.data)
  }
})
// --------------------------- 删除学员 -----------------------------------
// --------------------------- 编辑学员 -----------------------------------